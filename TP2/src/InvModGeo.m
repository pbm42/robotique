function [ R ] = InvModGeo( X,Y,Z )
%INVMODGEO Summary of this function goes here
%   Detailed explanation goes here

    a1=1; % longueur bras 1
    a2=1; % longueur bras 2
    d1=1; % hauteur base du robot
    d2=.05; % longueur bras 3
    
    rsqr=((X^2+Y^2-a1^2-a2^2)/(2*a1*a2));
    
    theata2=acos(rsqr);
    theata1=atan2(Y,X)-atan2(a2*sin(theata2),a1+a2*cos(theata2));
    
    T2=theata2*180/pi;
    T1=theata1*180/pi;
    d3=(d1-(Z)-d2);
    T4=0;
    
    R=[T1, T2, d3, T4];
end

