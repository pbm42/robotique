function [A1] = Distance( dist )
%DISTANCE Summary of this function goes here
%   Detailed explanation goes here

ScaraModel = vrworld('../Scarademo.wrl');
open(ScaraModel);
EndV1=ScaraModel.EndV1.translation;
EndV2=ScaraModel.EndV2.translation;
x1=EndV1(1); x2=EndV2(1);
y1=-.5*dist; y2=.5*dist;

A1 = [x1, y1, 0, x2, y2, 0];
end

