function [q] = compute_command_sequence(x1, y1, angle, x2, y2, stepCount)
%se baser sur la simulationCarre
Te=0.1;  %periode d'echantillonnage

q(1,1)= x1;%5 * cos(0) + x1;    %x_o
q(1,2)= y1;%5 * sin(0) + y1;     %y_o
q(1,3)=angle;   %theta_o (radiant)

K1 = 1;
K2 = 1;

t(1)=0;

Na=stepCount;
l = 1;

startX = x1;
startY = y1;
goalX = x2;
goalY = y2;


wayLength = sqrt((goalX - startX)^2 + (goalY - startY)^2);

distA = x2 - startX;
distB = y2 - startY;
omegaBase = 0;

if (distB < 0)
  omegaBase = degtorad(180 * asin(distA / sqrt(distA.^2 + distB.^2)) / pi + 270);
else
  omegaBase = degtorad(180 * acos(distA / sqrt(distA.^2 + distB.^2)) / pi);
end
omegaBase = omegaBase - angle;
NbIterRotate = Na / 10;

for i=2:NbIterRotate,
   v=0;
   omega= omegaBase / ((NbIterRotate - 1)*Te);
   q(i,1)=q(i-1,1)+v*cos(q(i-1,3))*Te;
   q(i,2)=q(i-1,2)+v*sin(q(i-1,3))*Te;
   q(i,3)=q(i-1,3)+omega*Te;
   t(i)=0+i*Te;
end

NbItere=Na-NbIterRotate;
for i=NbIterRotate+1:Na,
    v = wayLength / (Na-NbIterRotate+1) / Te;
    omega=0;
    q(i,1)=q(i-1,1)+v*cos(q(i-1,3))*Te;
    q(i,2)=q(i-1,2)+v*sin(q(i-1,3))*Te;
    q(i,3)=q(i-1,3)+omega*Te;
    t(i)=0+i*Te;
end
end