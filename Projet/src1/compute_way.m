function [seq, nodeCount] = compute_way()

% constants
randomPointCount = 80;

leftBound = 0;
rightBound = 100;
bottomBound = 0;
topBound = 100;

neighborCount = 5;

startPoint = [0 0];
goalPoint = [100 100];

% collision shapes
shapeCount = 5;
shapes = cell(1, shapeCount);
shapes{1} = [10 10; 30 10; 30 20; 10 20];
shapes{2} = [40 40; 70 10; 70 20; 40 50];
shapes{3} = [50 60; 80 60; 80 90; 50 90];
shapes{4} = [20 70; 40 70; 40 95; 20 95];
shapes{5} = [75 20; 95 20; 95 40; 75 40];

%actual shapes
margin = 2;
actualShapes = shapes;
actualShapes{1}(1, :) = actualShapes{1}(1, :) + [margin margin];
actualShapes{1}(2, :) = actualShapes{1}(2, :) + [-margin margin];
actualShapes{1}(3, :) = actualShapes{1}(3, :) + [-margin -margin];
actualShapes{1}(4, :) = actualShapes{1}(4, :) + [margin -margin];

actualShapes{2}(1, :) = actualShapes{2}(1, :) + [margin margin];
actualShapes{2}(2, :) = actualShapes{2}(2, :) + [-margin margin*2];
actualShapes{2}(3, :) = actualShapes{2}(3, :) + [-margin -margin];
actualShapes{2}(4, :) = actualShapes{2}(4, :) + [margin -margin*2];

actualShapes{3}(1, :) = actualShapes{3}(1, :) + [margin margin];
actualShapes{3}(2, :) = actualShapes{3}(2, :) + [-margin margin];
actualShapes{3}(3, :) = actualShapes{3}(3, :) + [-margin -margin];
actualShapes{3}(4, :) = actualShapes{3}(4, :) + [margin -margin];

actualShapes{4}(1, :) = actualShapes{4}(1, :) + [margin margin];
actualShapes{4}(2, :) = actualShapes{4}(2, :) + [-margin margin];
actualShapes{4}(3, :) = actualShapes{4}(3, :) + [-margin -margin];
actualShapes{4}(4, :) = actualShapes{4}(4, :) + [margin -margin];

actualShapes{5}(1, :) = actualShapes{5}(1, :) + [margin margin];
actualShapes{5}(2, :) = actualShapes{5}(2, :) + [-margin margin];
actualShapes{5}(3, :) = actualShapes{5}(3, :) + [-margin -margin];
actualShapes{5}(4, :) = actualShapes{5}(4, :) + [margin -margin];


figure;
hold on;
axis([leftBound - 20, rightBound + 20, bottomBound - 20, topBound + 20]);

% plot shapes
for i = 1 : shapeCount
    vertexCount = size(shapes{i}, 1);
    plot(shapes{i}(:, 1), shapes{i}(:, 2), '-g');
    plot([shapes{i}(vertexCount, 1) shapes{i}(1, 1)], [shapes{i}(vertexCount, 2) shapes{i}(1, 2)], '-g');
end
% plot actual shapes
for i = 1 : shapeCount
    vertexCount = size(actualShapes{i}, 1);
    plot(actualShapes{i}(:, 1), actualShapes{i}(:, 2), '-r');
    plot([actualShapes{i}(vertexCount, 1) actualShapes{i}(1, 1)], [actualShapes{i}(vertexCount, 2) actualShapes{i}(1, 2)], '-r');
end
pause;


% points are generated randomly
randomPoints = [ones(randomPointCount, 1) * leftBound + randi(rightBound - leftBound, randomPointCount, 1), ones(randomPointCount, 1) * bottomBound + randi(topBound - bottomBound, randomPointCount, 1)];

% points within collision shapes are removed
in=zeros(size(randomPoints, 1), 1);
for i=1:shapeCount
    inTmp = inpolygon(randomPoints(:, 1), randomPoints(:, 2), shapes{i}(:, 1), shapes{i}(:, 2));
    in = in + inTmp;
end

filteredPointIndices = find(in <= 0);
filteredPointCount = size(filteredPointIndices, 1);

filteredPoints = zeros(filteredPointCount, 1);
for i=1:filteredPointCount
   index = filteredPointIndices(i);
   filteredPoints(i, 1) = randomPoints(index, 1);
   filteredPoints(i, 2) = randomPoints(index, 2);
end

% plot points
plot(filteredPoints(:, 1), filteredPoints(:, 2), 'o');
pause;

% add start and goal points
startPointIndex = filteredPointCount + 1;
goalPointIndex = filteredPointCount + 2;
filteredPoints(startPointIndex, :) = startPoint;
filteredPoints(goalPointIndex, :) = goalPoint;

filteredPointCount = filteredPointCount + 2;

% plot start and goal points
plot(startPoint(1), startPoint(2), 'o -r');
plot(goalPoint(1), goalPoint(2), 'o -g');
pause;

% distance matrix
distMatrix = zeros(filteredPointCount);

% adjacency matrix
adjMatrix = zeros(filteredPointCount);

% compute distances between each point pair
for i=1:filteredPointCount    
    for j=1:filteredPointCount
        p1 = filteredPoints(i, :);
        p2 = filteredPoints(j, :);
        distMatrix(i, j) = pdist([p1; p2], 'euclidean');
    end
end

% find closest neighbors of each point
for i=1:filteredPointCount
    for k=1:neighborCount
        [minValue, minIndex] = min(distMatrix(i, i+1:filteredPointCount));
        minIndex = minIndex + i;
        if (minValue ~= Inf)
            adjMatrix(i, minIndex) = minValue;
            adjMatrix(minIndex, i) = minValue;
            distMatrix(i, minIndex) = Inf;
        end
    end
end

% filter edges that intersect collision shapes
for i=1:filteredPointCount
    for j=1:filteredPointCount
        if (adjMatrix(i, j) > 0)
            p1 = filteredPoints(i, :);
            p2 = filteredPoints(j, :);
            for k=1:shapeCount
                [x, y] = polyxpoly([p1(1) p2(1)], [p1(2) p2(2)], shapes{k}(:, 1), shapes{k}(:, 2));
                if (size(x, 1) > 0)
                    adjMatrix(i, j) = 0;
                    adjMatrix(j, i) = 0;
                    break;
                end
            end
        end
    end
end

% plot graph
for i = 1 : filteredPointCount
    for j = 1 : filteredPointCount
        if (adjMatrix(i, j) > 0)
            p1 = filteredPoints(i, :);
            p2 = filteredPoints(j, :);
            plot([p1(1) p2(1)], [p1(2) p2(2)], '-b');
        end
    end
end
pause;

IMatrix = Inf*ones(filteredPointCount);
NextMatrix = -1*ones(filteredPointCount);

for i=1:filteredPointCount,
    for j=1:filteredPointCount,
        if i==j,
            IMatrix(i,j) = 0;
        elseif (adjMatrix(i,j) > 0),
            IMatrix(i,j) = adjMatrix(i,j);
        end                
    end
end

for k=1:filteredPointCount,
    for i=1:filteredPointCount,
        for j=1:filteredPointCount,
            ikj = IMatrix(i,k) + IMatrix(k,j);
            if  ikj < IMatrix(i,j),
                IMatrix(i,j) = ikj;
                NextMatrix(i,j) = k;
            end
        end
    end
end

seqIndices = getPathRecursiv(startPointIndex, goalPointIndex, IMatrix, NextMatrix)

nodeCount = size(seqIndices, 2) + 2;

seq = zeros(nodeCount, 2);
seq(1, :) = filteredPoints(startPointIndex);
seq(nodeCount, :) = filteredPoints(goalPointIndex);
for i = 2 : nodeCount - 1
    seq(i, 1) = filteredPoints(seqIndices(1, i - 1), 1);
    seq(i, 2) = filteredPoints(seqIndices(1, i - 1), 2);
end

figure;
hold on;
axis([leftBound - 20, rightBound + 20, bottomBound - 20, topBound + 20]);

% plot shapes
for i = 1 : shapeCount
    vertexCount = size(shapes{i}, 1);
    plot(shapes{i}(:, 1), shapes{i}(:, 2), '-g');
    plot([shapes{i}(vertexCount, 1) shapes{i}(1, 1)], [shapes{i}(vertexCount, 2) shapes{i}(1, 2)], '-g');
end
% plot actual shapes
for i = 1 : shapeCount
    vertexCount = size(actualShapes{i}, 1);
    plot(actualShapes{i}(:, 1), actualShapes{i}(:, 2), '-r');
    plot([actualShapes{i}(vertexCount, 1) actualShapes{i}(1, 1)], [actualShapes{i}(vertexCount, 2) actualShapes{i}(1, 2)], '-r');
end

% plot points
plot(filteredPoints(:, 1), filteredPoints(:, 2), 'o');

% plot start and goal points
plot(startPoint(1), startPoint(2), 'o -r');
plot(goalPoint(1), goalPoint(2), 'o -g');


pause;
