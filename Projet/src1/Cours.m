% Te = 0.1s = 100ms
%                         > p?riode d'?chantillonage
% Te = 0.05s = 50 ms              t = i * Te
% 
% (x) : x point, y0 = vecteur y0
% Vg = (x) * x0 + (y) * y0 = v * cosTH * x0 + v * sinTH * y0
% v -> |    | -> x
% w -> |    | -> y
%      |    | -> Thetha

% ( (x) )       ( cTH   0   ) ( v )
% ( (y) )  =    ( sTH   0   ) (   )
% ( (TH) )      ( 0     1   ) ( w )

% Int?gration num?rique
% x ~= (x(k) - x(k-1)) / Te = v * cosTH * (k - 1)
% <=>
% x(k) = x(k-1) + Te * v * cosTH(k-1)
% y(k) = y(k-1) + Te * v * sinTH(k-1)
% Th(k) = Th(k-1) + Te * w
% v = 2m/s
% x = v * t + x0
% Th(deltaT) = Th(0) + w * deltaT

