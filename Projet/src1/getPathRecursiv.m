function [ T ] = getPathRecursiv( i, j, distMat, nextMat )
T = [];
intermediate = nextMat(i, j);
if distMat(i, j) == Inf,
     T;
elseif intermediate == -1,
     T;
else
 T = [getPathRecursiv(i, intermediate, distMat, nextMat) intermediate getPathRecursiv(intermediate, j, distMat, nextMat)]

end

